#include <stdio.h>
#include "mem.h"

int main() {
    fprintf(stderr, "Normal memory allocation success.\n");
    void *heap = heap_init(10000);
    void *add_block1 = my_malloc(100);
    void *add_block2 = my_malloc(200);
    void *add_block3 = my_malloc(1);
    void *add_block4 = my_malloc(400);
    void *add_block5 = my_malloc(50);
    void *add_block6 = my_malloc(200);
    void *add_block7 = my_malloc(40);
    void *add_block8 = my_malloc(2000);
    debug_heap(stderr, heap);
    fprintf(stderr, "Freeing one block from several allocated ones.\n");
    my_free(add_block6);
    debug_heap(stderr, heap);
    fprintf(stderr, "Freeing two blocks from several allocated ones.\n");
    my_free(add_block3);
    my_free(add_block2);
    debug_heap(stderr, heap);
    fprintf(stderr, "The memory has run out, the new memory region expands the old one.\n");
    void *add_block9 = my_malloc(20000);
    debug_heap(stderr, heap);
    return 0;
}
