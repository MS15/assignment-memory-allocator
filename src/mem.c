#define _DEFAULT_SOURCE

#include <unistd.h>
#include <stddef.h>
#include <sys/mman.h>
#include "mem_internals.h"
#include "mem.h"
#include "util.h"


void debug_block(struct block_header *b, const char *fmt, ...);

void debug(const char *fmt, ...);

extern inline block_size size_from_capacity(block_capacity cap);

extern inline block_capacity capacity_from_size(block_size sz);

static bool block_is_big_enough(struct block_header *block, size_t query) {
    return block->capacity.bytes >= query;
}

static size_t pages_count(size_t mem) { return mem / getpagesize() + ((mem % getpagesize()) > 0); }

static size_t round_pages(size_t mem) { return getpagesize() * pages_count(mem); }

static void block_init(void *restrict addr, block_size block_sz, void *restrict next) {
    *((struct block_header *) addr) = (struct block_header) {
            .next = next,
            .capacity = capacity_from_size(block_sz),
            .is_free = true
    };
}

extern inline bool region_is_invalid(const struct region *r);


static void *map_pages(void const *addr, size_t length, int additional_flags) {
    return mmap((void *) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, 0, 0);
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region(void const *addr, size_t query) {
    query = size_max(round_pages(query), REGION_MIN_SIZE);
    struct region new_region;
    void *new_pages = map_pages(addr, query, MAP_FIXED);
    if (new_pages == MAP_FAILED) {
        new_pages = map_pages(addr, query, 0);
        new_region = (struct region) {new_pages, query, false};
    } else
        new_region = (struct region) {new_pages, query, true};
    block_init(new_pages, (block_size) {query}, NULL);
    return new_region;
}

static void *block_after(struct block_header const *block);

void *heap_init(size_t initial) {
    const struct region region = alloc_region(HEAP_START, initial);
    if (region_is_invalid(&region)) return NULL;
    return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable(struct block_header *restrict block, size_t query) {
    return block->is_free &&
           query + offsetof(struct block_header, contents) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big(struct block_header *block, size_t query) {
    if (block_splittable(block, query)) {
        void *new_block_addr = block->contents + query;
        block_init(new_block_addr,
                   (block_size) {block->capacity.bytes - query},
                   block->next);
        block->capacity.bytes = query;
        block->next = (struct block_header *) new_block_addr;
        return true;
    }
    return false;
}


/*  --- Слияние соседних свободных блоков --- */
//получение блока следующего за текущим
static void *block_after(struct block_header const *block) {
    return (void *) (block->contents + block->capacity.bytes);
}

//проверка идут ли блоки друг за другом
static bool blocks_continuous(struct block_header const *fst,
                              struct block_header const *snd) {
    return (void *) snd == block_after(fst);
}

//проверка можно ли блоки соеденить
static bool mergeable(struct block_header const *restrict fst,
                      struct block_header const *restrict snd) {
    return fst->is_free && snd->is_free && blocks_continuous(fst, snd);
}

//слияние блока со следующим
static bool try_merge_with_next(struct block_header *block) {
    struct block_header *next_block = block->next;
    if (next_block && mergeable(block, next_block)) {
        block->next = next_block->next;
        block->capacity.bytes += size_from_capacity(next_block->capacity).bytes;
        return true;
    }
    return false;
}

/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
    enum {
        BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND
    } type;
    struct block_header *block;
};

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing(struct block_header *block, size_t sz) {
    while (true) {
        if (block->is_free) {
            while (try_merge_with_next(block));
            if (block_is_big_enough(block, sz)) {
                split_if_too_big(block, sz);
                block->is_free = false;
                return (struct block_search_result) {BSR_FOUND_GOOD_BLOCK, block};
            }
        }
        if (!block->next)
            break;
        else
            block = block->next;
    }
    return (struct block_search_result) {BSR_REACHED_END_NOT_FOUND, block};
}

static bool grow_heap(struct block_header *restrict last, size_t query) {
    const struct region region = alloc_region(last->contents + last->capacity.bytes,
                                              size_from_capacity((block_capacity) {query}).bytes);
    if (region_is_invalid(&region)) return false;
    last->next = region.addr;
    try_merge_with_next(last);
    return true;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header *memalloc(struct block_header *heap_start, size_t query) {
    query = size_max(query, BLOCK_MIN_CAPACITY);
    struct block_search_result new_block = try_memalloc_existing(heap_start, query);
    if (new_block.type == BSR_FOUND_GOOD_BLOCK)
        return new_block.block;
    if (new_block.type == BSR_REACHED_END_NOT_FOUND && grow_heap(new_block.block, query))
        return try_memalloc_existing(heap_start, query).block;
    return NULL;
}

void *my_malloc(size_t query) {
    struct block_header *const addr = memalloc((struct block_header *) HEAP_START, query);
    if (addr) return addr->contents;
    else return NULL;
}

static struct block_header *block_get_header(void *contents) {
    return (struct block_header *) (((uint8_t *) contents) - offsetof(struct block_header, contents));
}

void my_free(void *mem) {
    if (!mem) return;
    struct block_header *header = block_get_header(mem);
    header->is_free = true;
    while (try_merge_with_next(header));
}
